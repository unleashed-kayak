#!/usr/bin/bash

# {{{ CDDL HEADER
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
# }}}

#
# Copyright 2017 OmniTI Computer Consulting, Inc. All rights reserved.
# Copyright 2018 OmniOS Community Edition (OmniOSce) Association.
#

#
# Build an ISO installer using the Kayak tools.
#

if [ `id -u` != "0" ]; then
	echo "You must be root to run this script."
	exit 1
fi

if [ -z "$BUILDSEND_MP" ]; then
	echo "Using /rpool/kayak_image for BUILDSEND_MP"
	BUILDSEND_MP=/rpool/kayak_image
fi

if [ -z "$VERSION" ]; then
	echo "VERSION not set." >&2
	exit 1
fi
echo "Using version $VERSION..."

stage()
{
	echo "***"
	echo "*** $*"
	echo "***"
}

# Allow temporary directory override
: ${TMPDIR:=/tmp}

KAYAK_MINIROOT=$BUILDSEND_MP/boot_archive.gz
ZFS_IMG=$BUILDSEND_MP/kayak_${VERSION}.zfs

[ ! -f $KAYAK_MINIROOT -o ! -f $ZFS_IMG ] && echo "Missing files." && exit 1

ISO_ROOT=$TMPDIR/iso_root
BA_ROOT=$TMPDIR/boot_archive

MNT=/mnt
DST_ISO=$BUILDSEND_MP/${VERSION}.iso

#############################################################################
#
# The kayak mini-root is used for both the ISO root filesystem and for the
# miniroot that is loaded and mounted on / when booted.
#

set -o errexit

mkdir $ISO_ROOT

stage "Adding files to ISO root"
# our tar(1) doesn't seem to support multiple file args with -C...
tar -cf - -C $BUILDSEND_MP/root boot | tar -xf - -C $ISO_ROOT
tar -cf - -C $BUILDSEND_MP/root platform | tar -xf - -C $ISO_ROOT

# Place the full ZFS image into the ISO root so it does not form part of the
# boot archive (otherwise the boot seems to hang for several minutes while
# the miniroot is loaded)

stage "Adding ZFS image to ISO root"
pv $ZFS_IMG > $ISO_ROOT/unleashed.zfs
# Create a file to indicate that this is the right volume set on which to
# find the image - see src/mount_media.c
echo "unleashed-installer" > $ISO_ROOT/.volsetid

# Remind people this is the installer.
cat <<EOF > $ISO_ROOT/boot/loader.conf.local
loader_menu_title="Welcome to the Unleashed installer"
autoboot_delay=10
ttya-mode="115200,8,n,1,-"
console="text,ttya"
EOF

#
# Install the miniroot $ISO_ROOT as the boot archive.
#
stage "Installing boot archive"
cp $KAYAK_MINIROOT ${KAYAK_MINIROOT%.gz}.hash $ISO_ROOT/platform/
stage "ISO root size: `du -sh $ISO_ROOT/.`"

# And finally, burn the ISO.
LC_ALL=C mkisofs -N -l -R -U -d -D \
	-o $DST_ISO \
	-b boot/cdboot \
	-c .catalog \
	-no-emul-boot \
	-boot-load-size 4 \
	-boot-info-table \
	-allow-multidot \
	-no-iso-translate \
	-cache-inodes \
	-V "Unleashed $VERSION" \
	$ISO_ROOT

rm -rf $ISO_ROOT
stage "$DST_ISO is ready"
ls -lh $DST_ISO

# Vim hints
# vim:fdm=marker
