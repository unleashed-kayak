Using Kayak
===========

Building
--------

 * Set `PKGURL1` to point to the pkg repo with the unleashed publisher.
 * Set `PKGURL2` to point to the pkg repo with the userland publisher.
 * Set `VERSION` to the desired version (e.g., "v1.0")

 * zfs create rpool/kayak_image
 * gmake install-usb BUILDSEND=rpool/kayak_image

Operation
---------

DHCP server:
 * your server should be set to PXE boot
 * the DHCP server must return and IP, a nextserver and a bootfile
 * the boot file should be the file {{{/boot/grub/pxegrub}}} or {{{/boot/pxeboot}}} from an existing OmniOS system

TFTP server:
 * menu.lst.01<macaddr_just_hex_caps> based on the template provided in this directory should be placed in /tftpboot/ if using grub, or put loader.conf.local in /tftpboot/boot
 * /boot/grub/pxegrub or /boot/pxeboot should be placed in /tftpboot/
 * /platform/kernel/unix should be placed in /tftpboot/omnios/kayak/
 * the miniroot.gz file should be placed in /tftpboot/omnios/kayak/

HTTP server:
 * The system install images should be placed an accessible URL
 * The target system kayak config should be placed at a URL path with the filename <macaddr_just_hex_caps>

