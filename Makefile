#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2017 OmniTI Computer Consulting, Inc.  All rights reserved.
#

VERSION?=$(shell awk '$$1 == "unleashed" { print $$2 }' /etc/release)
BUILDSEND=rpool/kayak_image

BUILDSEND_MP=$(shell zfs get -o value -H mountpoint $(BUILDSEND))

all:

anon.dtrace.conf:
	pfexec dtrace -A -q -n 'int seen[string]; fsinfo:::lookup,fsinfo:::open /args[0]->fi_mount=="/" && seen[args[0]->fi_pathname]==0/{printf("%s\n", args[0]->fi_pathname); seen[args[0]->fi_pathname]=1; } fbt::mmapobj_map_elf:entry /seen[stringof(((struct vnode*)arg0)->v_path)]==0/ { printf("%s\n", stringof(((struct vnode *)arg0)->v_path)); }' -o $@.tmp
	cat /kernel/drv/dtrace.conf $@.tmp > $@
	rm $@.tmp

MINIROOT_DEPS=install.sh ksh.profile anon.dtrace.conf anon.system

$(BUILDSEND_MP)/kayak_$(VERSION).zfs:	build_zfs_send.sh
	@test -d "$(BUILDSEND_MP)" || (echo "$(BUILDSEND) missing" && false)
	./build_zfs_send.sh -d $(BUILDSEND) $(VERSION)

build_image.sh:
	VERSION=$(VERSION) ./build_image.sh

build_zfs_send.sh:
	VERSION=$(VERSION) ./build_zfs_image.sh

$(BUILDSEND_MP)/miniroot.gz:	$(MINIROOT_DEPS)
	if test -n "`zfs list -H -t snapshot $(BUILDSEND)/root@fixup 2>/dev/null`"; then \
	  VERSION=$(VERSION) DEBUG=$(DEBUG) ./build_image.sh $(BUILDSEND) fixup ; \
	else \
	  VERSION=$(VERSION) DEBUG=$(DEBUG) ./build_image.sh $(BUILDSEND) begin ; \
	fi

mount_media:	src/mount_media.c
	gcc -o $@ $< -ldevinfo

install-iso: $(BUILDSEND_MP)/$(VERSION).iso
$(BUILDSEND_MP)/$(VERSION).iso: 	mount_media $(BUILDSEND_MP)/miniroot.gz $(BUILDSEND_MP)/kayak_$(VERSION).zfs
	BUILDSEND_MP=$(BUILDSEND_MP) VERSION=$(VERSION) ./build_iso.sh

install-usb:	$(BUILDSEND_MP)/$(VERSION).img
$(BUILDSEND_MP)/$(VERSION).img: $(BUILDSEND_MP)/$(VERSION).iso
	./usbgen.sh $(BUILDSEND_MP)/$(VERSION).iso $(BUILDSEND_MP)/$(VERSION).img /tmp
