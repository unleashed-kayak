#!/bin/sh -e

if ! [ -f /.cdrom/.volsetid ]; then
    devfsadm
    mkdir -p /.cdrom
    /mount_media unleashed-installer || {
        echo "Error: Unable to mount install media. Exiting." >&2
        exit 1
    }
    # diskinfo can fail to list virtio devices on the first run, so run it here
    # for what seems no reason
    diskinfo >/dev/null
fi

while :; do
    read 'hostname?System hostname? '
    if [ -n "$hostname" ]; then break; fi
done
while :; do
    availdisks=$(diskinfo -H | awk '{ printf $2 " " }')
    if [ -z "$availdisks" ]; then
        availdisks="none"
        defdisk=
    else
        availdisks="${availdisks% }"
        defdisk=${availdisks%% *}
    fi
    echo "Available disks are: ${availdisks}."
    read "disks?Which disks should be used for installation? [$defdisk] ('?' for details) "
    case "$disks" in
        '?') diskinfo; continue;;
        '') disks=$defdisk; if [ -z "$defdisk" ]; then continue; fi; break;;
    esac;
    for d in $disks; do
        for ad in $availdisks; do
            if [ $d = $ad ]; then continue 2; fi
        done
        echo "'$d' is not a valid disk."
        continue 2
    done
    break
done
case $disks in 
    *" "*) disks="mirror $disks" ;;
esac
zpool create -O compression=on -f rpool $disks
zfs create -o canmount=off -o mountpoint=legacy rpool/ROOT
echo "Installing image."
pv -B 128m -w 78 < /.cdrom/unleashed.zfs | zfs receive -u rpool/ROOT/unleashed
zfs destroy rpool/ROOT/unleashed@kayak
zfs set canmount=noauto mountpoint=legacy rpool/ROOT/unleashed
zfs create -o mountpoint=/home rpool/home
beadm mount unleashed /mnt >/dev/null
devfsadm -r /mnt
echo "$hostname" >/mnt/etc/nodename
ln -sf generic_limited_net.xml /mnt/etc/svc/profile/generic.xml
# empty string password
sed -i '' 's,^root:NP:,root:$2b$06$./nqDdz5G40L1hjMLByHSOpA7GLmRJb4xU2n/ncw0jBKd3It8kcWm:,' /mnt/etc/shadow
bootadm install-bootloader -M -f -R /mnt -P rpool
bootadm update-archive -R /mnt
umount /mnt
zpool set bootfs=rpool/ROOT/unleashed rpool
beadm activate unleashed >/dev/null
cat <<EOF

Congratulations, your $(uname -sr) installation has successfully completed.

Type 'reboot' at the prompt to boot the new system.
EOF
