#!/bin/bash

#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2017 OmniTI Computer Consulting, Inc.  All rights reserved.
#

fail() {
	echo "ERROR: $*"
	exit 1
}

# NOTE --> The URL needs to be updated with every release.
# Change "bloody" to whatever release the current branch is.
PUBLISHER=unleashed
PKGURL1=${PKGURL1-/usr/nightly/packages/i386/nightly/repo.redist}
PKGURL2=${PKGURL2-/ws/oi-userland/amd64/repo}
: ${GZIP_CMD:=gzip}
SRCDIR=$(dirname $0)
DIDWORK=0
BUILDNUM=${VERSION//r/}
if [[ ${SRCDIR:0:1} != "/" ]]; then
  SRCDIR=`pwd`/$SRCDIR
fi
if [[ -z "${1}" ]]; then
  echo "$0 <zfs pool> [checkpoint]"
  exit 1
else
  BASE=${1}
  shift
  BASEDIR=`zfs get -o value -H mountpoint $BASE`
fi
MKFILEDIR=/tmp
WORKDIR=$BASEDIR
ROOTDIR=$WORKDIR/root
SVCCFG_DTD=${ROOTDIR}/usr/share/lib/xml/dtd/service_bundle.dtd.1
SVCCFG_REPOSITORY=${ROOTDIR}/etc/svc/repository.db
if [[ -f ${PREBUILT_ILLUMOS}/usr/src/cmd/svc/svccfg/svccfg-native ]]; then
	SVCCFG=${PREBUILT_ILLUMOS}/usr/src/cmd/svc/svccfg/svccfg-native
else
	echo "WARNING -- Not using 'native' svccfg, may hang on build."
	echo "       We recommend a pre-built illumos's svccfg-native."
	echo "       Set PREBUILT_ILLUMOS in your environment to point"
	echo "       to a built illumos-omnios repository."
	SVCCFG=/usr/sbin/svccfg
fi
export WORKDIR ROOTDIR SVCCFG_DTD SVCCFG_REPOSITORY SVCCFG

# This was uber-helpful
# http://alexeremin.blogspot.com/2008/12/preparing-small-miniroot-with-zfs-and.html

PKG=/bin/pkg

UNNEEDED_MANIFESTS="application/management/net-snmp.xml
	application/pkg/pkg-server.xml application/pkg/pkg-mdns.xml
	system/rmtmpfiles.xml system/mdmonitor.xml
	system/fm/notify-params.xml system/device/allocate.xml
	system/device/devices-audio.xml system/auditd.xml
	system/metasync.xml system/pkgserv.xml system/fcoe_initiator.xml
	system/metainit.xml system/zonestat.xml
	system/cron.xml system/rbac.xml system/sac.xml
	system/auditset.xml system/hotplug.xml
	system/wusb.xml system/zones.xml
	system/intrd.xml system/coreadm.xml
	system/extended-accounting.xml
	system/scheduler.xml
	system/logadm-upgrade.xml system/resource-mgmt.xml
	system/idmap.xml
	network/ldap/client.xml network/shares/reparsed.xml
	network/shares/group.xml network/inetd-upgrade.xml
	network/smb/client.xml network/smb/server.xml
	network/network-iptun.xml network/ipsec/policy.xml
	network/ipsec/ipsecalgs.xml network/ipsec/ike.xml
	network/ipsec/manual-key.xml network/forwarding.xml
	network/inetd.xml network/npiv_config.xml
	network/ssl/kssl-proxy.xml network/rpc/metamed.xml
	network/rpc/mdcomm.xml network/rpc/gss.xml
	network/rpc/bind.xml network/rpc/keyserv.xml
	network/rpc/meta.xml network/rpc/metamh.xml
	network/socket-filter-kssl.xml network/network-netcfg.xml
	network/nfs/status.xml network/nfs/cbd.xml
	network/nfs/nlockmgr.xml network/nfs/mapid.xml
	network/nfs/client.xml network/network-ipqos.xml
	network/security/ktkt_warn.xml network/security/krb5kdc.xml
	network/security/kadmin.xml network/network-install.xml
	network/bridge.xml network/network-initial.xml
	network/network-ipmgmt.xml network/routing/legacy-routing.xml
	network/network-service.xml network/network-physical.xml
	network/network-netmask.xml network/dlmgmt.xml
	network/network-location.xml network/ibd-post-upgrade.xml
	network/network-routing-setup.xml network/network-loopback.xml
	network/dns/client.xml network/dns/install.xml
	network/dns/multicast.xml platform/i86pc/acpihpd.xml
	system/hostid.xml system/power.xml system/pfexecd.xml
	system/consadm.xml system/pools.xml system/console-login.xml
	system/stmf.xml system/fmd.xml system/utmp.xml
	system/poold.xml system/dumpadm.xml"

SYSTEM="system/boot/real-mode
	system/boot/loader system/data/hardware-registry
	system/extended-system-utilities
	system/file-system/autofs system/file-system/nfs
	system/file-system/udfs
	system/flash/fwflash
	system/ipc
	system/library/policykit
	system/library/processor
	system/library/storage/fibre-channel/hbaapi
	system/library/storage/fibre-channel/libsun_fc
	system/library/storage/ima/header-ima
	system/library/storage/ima
	system/library/storage/libmpapi
	system/library/storage/libmpscsi_vhci
	system/network
	system/storage/luxadm
	system/storage/fibre-channel/port-utility"

DEBUG_PKGS="developer/debug/mdb"

PARTS="system/core-os system/kernel
	shell/pipe-viewer editor/vim web/curl
	developer/linker openssh
	diagnostic/diskinfo shell/bash"

PKGS="$PARTS $SYSTEM"

if [ -n "$DEBUG" ]; then
	PKGS="$PKGS $DEBUG_PKGS"
	BIGROOT=1
fi
CULL="python package/pkg snmp"

ID=`id -u`
if [[ "$ID" != "0" ]]; then
	echo "must run as root"
	exit 1
fi

chkpt() {
	SNAP=`zfs list -H -t snapshot $BASE/root@${1} 2> /dev/null`
	if [[ "$DIDWORK" -ne "0" ]]; then
		if [[ -n "$SNAP" ]]; then
			zfs destroy $BASE/root@${1} || \
				fail "zfs destroy ${1} failed"
		fi
		zfs snapshot $BASE/root@${1} || fail "zfs snapshot failed"
	fi
	if [[ "${1}" != "begin" ]]; then
		echo " === Proceeding to phase $1 (zfs @${1}) ==="
		zfs rollback -r $BASE/root@${1} || fail "zfs rollback failed"
	else
		echo " === Proceeding to phase $1 ==="
	fi
	CHKPT=$1
	DIDWORK=1
}

if [[ -n "$1" ]]; then
	echo "Explicit checkpoint requested: '$1'"
	CHKPT=$1
	chkpt $CHKPT
fi
if [[ -z "$CHKPT" ]]; then
	CHKPT="begin"
fi

declare -A keep_list
load_keep_list() {
	for datafile in $*
	do
		FCNT=0
		while read file
		do
			if [[ -n "$file" ]]; then
				keep_list+=([$file]="x")
				FCNT=$(($FCNT + 1))
			fi
		done < <(cut -f2- -d/ $datafile)
		echo " --- keeping $FCNT files from $datafile"
	done
}

step() {
	CHKPT=""
	case "$1" in

	"begin")
	zfs destroy -r $BASE/root 2> /dev/null
	zfs create -o compression=off $BASE/root || fail "zfs create failed"
	chkpt pkg
	;;

	"pkg")

	echo "Creating image of $PUBLISHER from $PKGURL1"
	$PKG image-create -F -p $PUBLISHER=$PKGURL1 $ROOTDIR || fail "image-create"
        $PKG -R $ROOTDIR change-variant arch=i386 # FIXME hack
        $PKG -R $ROOTDIR set-publisher -p $PKGURL2 || fail 'userland'
	$PKG -R $ROOTDIR install $PKGS || fail "install"
	chkpt fixup
	;;

	"fixup")

	echo "Fixing up install root"
	(cp $ROOTDIR/etc/vfstab $WORKDIR/vfstab && \
		awk '{if($3!="/"){print;}}' $WORKDIR/vfstab > $ROOTDIR/etc/vfstab && \
		echo "/devices/ramdisk:a - / ufs - no nologging" >> $ROOTDIR/etc/vfstab) || \
		fail "vfstab / updated"
	rm $WORKDIR/vfstab
	cp $ROOTDIR/lib/svc/seed/global.db $ROOTDIR/etc/svc/repository.db

	sed -i '' 's,PASSREQ=YES,PASSREQ=NO,' $ROOTDIR/etc/default/login

	${SVCCFG} import ${ROOTDIR}/lib/svc/manifest/milestone/sysconfig.xml
	for xml in $UNNEEDED_MANIFESTS; do
		rm -f ${ROOTDIR}/lib/svc/manifest/$xml && echo " --- tossing $xml"
	done
	echo " --- initial manifest import"
	# See if we can transform manifest-import to use the 'native' svccfg.
	sed 's/\/usr\/sbin\/svccfg/\$SVCCFG/g' \
	    < ${ROOTDIR}/lib/svc/method/manifest-import \
	    > /tmp/manifest-import.$$
	chmod 0755 /tmp/manifest-import.$$
	export SVCCFG
	/tmp/manifest-import.$$ -f ${ROOTDIR}/etc/svc/repository.db \
		-d ${ROOTDIR}/lib/svc/manifest
	/bin/rm -f /tmp/manifest-import.$$

	${SVCCFG} -s 'system/boot-archive' setprop 'start/exec=:true'
	${SVCCFG} -s 'system/manifest-import' setprop 'start/exec=:true'
        ${SVCCFG} -s 'system/console-login' setprop 'start/exec=:true'

        cat > $ROOTDIR/etc/inittab <<EOF
ap::sysinit:/sbin/autopush -f /etc/iu.ap
sp::sysinit:/sbin/soconfig -d /etc/sock2path.d
smf::sysinit:/lib/svc/bin/svc.startd
sh::respawn:/bin/sh -l </dev/console >/dev/console 2>/dev/console
EOF

        echo "#!/bin/sh" > ${ROOTDIR}/lib/svc/method/manifest-import
        echo "exit 0" >> ${ROOTDIR}/lib/svc/method/manifest-import
	chmod 555 ${ROOTDIR}/lib/svc/method/manifest-import
	chkpt cull
	;;

	"cull")
	if [[ -z "$BIGROOT" ]]; then
		load_keep_list data/*
		while read file
		do
			if [[ -n "$file" && \
			      ${keep_list[$file]} == "" && \
			      -e "$ROOTDIR/$file" && \
			      ! -d $ROOTDIR/$file ]] ; then
				rm -f $ROOTDIR/$file
			fi
		done < <(cd $ROOTDIR && find ./ | cut -c3-)
	fi

	chkpt mkfs
	;;

	"mkfs")
	size=`zfs get -o value -Hp logicalreferenced $BASE/root`
	size=$((size/1024 + size/1024/10))
	echo " --- making miniroot ufs image of size ${size}k"
	/usr/sbin/mkfile ${size}k $MKFILEDIR/miniroot || fail "mkfile"
	lofidev=`/usr/sbin/lofiadm -a $MKFILEDIR/miniroot`
	rlofidev=`echo $lofidev |sed s/lofi/rlofi/`
	yes | /usr/sbin/newfs -m 0 $rlofidev 2> /dev/null > /dev/null || fail "newfs"
	chkpt mount
	;;

	"mount")
	mkdir -p $WORKDIR/mnt
	/usr/sbin/mount -o nologging $lofidev $WORKDIR/mnt || fail "mount"
	chkpt copy
	;;

	"copy")
	pushd $ROOTDIR >/dev/null
	/usr/bin/find . -not '(' -path ./var/pkg/'*' -or -path \
		./usr/share/man/'*' -or -path ./usr/share/doc/'*' -or -path \
		./usr/lib/python2.7/'*' -or -path  ./usr/share/vim/'*' -or \
		-path ./usr/share/zoneinfo/'*' ')' \
		| /usr/bin/cpio -pdum $WORKDIR/mnt >/dev/null || fail "populate root"
	/usr/sbin/devfsadm -r $WORKDIR/mnt > /dev/null
	popd >/dev/null

	pushd $SRCDIR >/dev/null
	install -m 0755 mount_media $WORKDIR/mnt
	install -m 0644 ksh.profile $WORKDIR/mnt/.profile
	install -m 0755 install.sh $WORKDIR/mnt/install
	popd >/dev/null

	sed -i '' -e 's%^root:NP:%root::%' $WORKDIR/mnt/etc/shadow

	if [[ -n "$DEBUG" ]]; then
		cp $SRCDIR/anon.system $WORKDIR/mnt/etc/system
		cp $SRCDIR/anon.dtrace.conf $WORKDIR/mnt/kernel/drv/dtrace.conf
	fi
	chkpt umount
	;;

	"umount")
	/usr/sbin/umount $WORKDIR/mnt || fail "umount"
	/usr/sbin/lofiadm -d $MKFILEDIR/miniroot || fail "lofiadm delete"
	chkpt compress
	;;

	"compress")
        digest -a sha1 $MKFILEDIR/miniroot > $WORKDIR/boot_archive.hash
	$GZIP_CMD -c -f $MKFILEDIR/miniroot > $WORKDIR/boot_archive.gz
	rm -f $MKFILEDIR/miniroot
	chmod 644 $WORKDIR/boot_archive*
	echo " === Finished ==="
	ls -l $WORKDIR/boot_archive.gz
	;;

	esac
}

while [[ -n "$CHKPT" ]]; do
	step $CHKPT
done
