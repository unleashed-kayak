# vim: ft=sh noet ts=8 sts=8 sw=8
set -o emacs

PATH=/usr/sbin:/sbin:/usr/bin

cat <<EOF
Welcome to the $(uname -sr) installer

EOF
while true ; do
	read 'reply?(I)nstall or (S)hell? '
	case "$reply" in
		[iI]*) /install && break ;;
		[sS]*) break ;;
	esac
done
